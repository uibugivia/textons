import numpy as np
import cv2
from .MSRCv2ImageHog import MSRCv2ImageHog

from skimage import feature

from Textons.Config import config


class HOGFeatures:
    """
    Class to generate and manage offsets as features in the hand RDF problem.
    """

    def __init__(self, all_offsets=config.OFFSETS_USE_ALL):
        """

        :param all_offsets: bool
        True if want to generate all offsets,
        False if only want to load the offsets specified in config.FILE_OFFSETS file
        """
        np.random.seed(config.OFFSETS_SEED)

        self._offsets_u = np.random.randint(-config.OFFSET_MAX, config.OFFSET_MAX, (config.N_FEATURES, 2))
        self._offsets_v = np.random.randint(-config.OFFSET_MAX, config.OFFSET_MAX, (config.N_FEATURES, 2))

        # TODO: idea! comparar bins aleatoris
        # TODO: idea! utilitzar diferents funcions per comparar els bins

        # self._feats_ch1 = np.random.randint(0, 3, config.N_FEATURES)
        # self._feats_ch2 = np.random.randint(0, 3, config.N_FEATURES)

        # self._feats_func = np.random.randint(0, 4, config.N_FEATURES)

    def get(self, im, labels):
        """
        Given a depth image and a set of (x, y) positions,
        get the features of the pixels in the image specified by the positions.

        :param im: MSRCv2ImageHog The image to use
        :param positions: List|ndarray of the (x, y) positions to compute the features.

        :return: Tuple list, ndarray
        A list with the positions of the features computed.
        An ndarray of shape (len(positions), len(offsets)) with the features computed in each position.

        """
        #
        # h, w = im.hog.shape[:2]
        # # nfeats = config.N_FEATURES * MSRCv2ImageHog.ORIENTATIONS
        # nfeats = config.N_FEATURES*9*2*2
        # features = np.zeros((h * w,  + 1), dtype=np.float64)
        #
        # margin = 8*2*config.OFFSET_MAX
        # gray = cv2.copyMakeBorder(im.gray, top=margin, bottom=margin, left=margin,
        #                           right=margin, borderType=cv2.BORDER_REFLECT)
        # hog, hogImage = feature.hog(gray, orientations=MSRCv2ImageHog.ORIENTATIONS,
        #                             pixels_per_cell=MSRCv2ImageHog.PIXELS_PER_CELL,
        #                             cells_per_block=MSRCv2ImageHog.CELLS_PER_BLOCK,
        #                             transform_sqrt=True, block_norm="L1", visualise=True,
        #                             feature_vector=False)
        #
        # hog *= im.gray.shape[0] * im.gray.shape[1]
        # for nfeat in range(config.N_FEATURES):
        #     y1, x1 = self._offsets_u[nfeat, :]
        #     y2, x2 = self._offsets_v[nfeat, :]
        #
        #     margin = config.OFFSET_MAX
        #     A = hog[margin + y1:margin + y1 + h, margin + x1:margin + x1 + w, :, :, :]
        #     B = hog[margin + y2:margin + y2 + h, margin + x2:margin + x2 + w, :, :, :]
        #
        #     feat = A - B
        #     for cell_x in range(2):
        #         for cell_y in range(2):
        #             for bin in range(9):
        #                 features[:, nfeat] = feat[:, :, cell_x, cell_y, bin].flatten()
        # features[:, -1] = im.getHogLabels().flatten()

        h, w = im.hog.shape[:2]
        nfeats = config.N_FEATURES
        features = np.zeros((h * w, nfeats + 1), dtype=MSRCv2ImageHog.FEATURES_DTYPE)

        margin = 8*2*config.OFFSET_MAX
        gray = cv2.copyMakeBorder(im.gray, top=margin, bottom=margin, left=margin,
                                  right=margin, borderType=cv2.BORDER_REFLECT)

        hog, hogImage = feature.hog(gray, orientations=MSRCv2ImageHog.ORIENTATIONS,
                                    pixels_per_cell=MSRCv2ImageHog.PIXELS_PER_CELL,
                                    cells_per_block=MSRCv2ImageHog.CELLS_PER_BLOCK,
                                    transform_sqrt=True,
                                    block_norm="L2-Hys",
                                    visualise=True,
                                    feature_vector=False)
        hog = np.reshape(hog, (hog.shape[0], hog.shape[1], 2 * 2 * 9))

        for nfeat in range(config.N_FEATURES):
            y1, x1 = self._offsets_u[nfeat, :]
            y2, x2 = self._offsets_v[nfeat, :]

            margin = config.OFFSET_MAX
            A = hog[margin + y1:margin + y1 + h, margin + x1:margin + x1 + w, :].astype(np.float32)
            B = hog[margin + y2:margin + y2 + h, margin + x2:margin + x2 + w, :].astype(np.float32)

            features[:, nfeat] = cv2.compareHist(A, B, cv2.HISTCMP_BHATTACHARYYA)

        features[:, -1] = im.getHogLabels().flatten()

        return features

    def random_features(self, im, labels, nsamples_class=config.DATA_PIXELS_CLASS):
        list_tag_feats = []
        tags = np.unique(labels)
        index = np.argwhere(tags == 0)
        tagsnoBG = np.delete(tags, index)
        print(tagsnoBG)
        if len(tagsnoBG) == 0:
            return None
        nsamples_tags = [nsamples_class]
        for t in tags:
            positions = np.nonzero(labels == t)
            positions_y, positions_x = positions
            nsamples_tags.append(len(positions_y))

        all_features = self.get(im, labels)
        nsamples = np.min(nsamples_tags)
        for i_tag, t in enumerate(tags):
            positions = np.nonzero(labels.flatten() == t)[0]
            idx_samples = np.random.randint(0, len(positions), nsamples)

            tag_features = np.zeros((nsamples, MSRCv2ImageHog.N_FEATURES + 1),
                                    dtype=MSRCv2ImageHog.FEATURES_DTYPE)
            i = 0
            for i_pos in idx_samples:
                tag_features[i, :] = all_features[positions[i_pos], :]
                i += 1
            list_tag_feats.append(tag_features)

        return np.concatenate(list_tag_feats)

