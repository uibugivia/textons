import numpy as np
import cv2

from skimage import feature

from DatasetsHelpers.DatasetsHelpers import VOC2012Image

from Textons.Config import config


class VOC2012ImageHog(VOC2012Image):
    PIXELS_PER_CELL = (8, 8)
    ORIENTATIONS = 9
    CELLS_PER_BLOCK = (2, 2)
    N_FEATURES = config.N_FEATURES
    FEATURES_DTYPE = np.float64

    def __init__(self, p_im):
        super().__init__(p_im)
        image = cv2.GaussianBlur(self.im_bgr, (5, 5), 0)
        if not self.isCorrect:
            return

        self.gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        self.hog, self.hogImage = feature.hog(self.gray, orientations=self.ORIENTATIONS,
                                              pixels_per_cell=self.PIXELS_PER_CELL,
                                              cells_per_block=self.CELLS_PER_BLOCK,
                                              transform_sqrt=True,
                                              block_norm="L2-Hys",
                                              visualise=True,
                                              feature_vector=False)

        self.__hogLabels = None

    def getHogLabels(self):
        if self.__hogLabels is None:
            h, w = self.hog.shape[:2]
            self.__hogLabels = np.zeros((h, w), dtype=np.uint8)
            for y in range(h):
                for x in range(w):
                    y_index = y * self.PIXELS_PER_CELL[0] + int(self.PIXELS_PER_CELL[0] / 2)
                    x_index = x * self.PIXELS_PER_CELL[1] + int(self.PIXELS_PER_CELL[1] / 2)
                    self.__hogLabels[y, x] = self.labels[y_index, x_index]
        return self.__hogLabels
