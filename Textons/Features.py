import cv2
import numpy as np

from Textons.Config import config


class Features:
    """
    Class to generate and manage offsets as features in the hand RDF problem.
    """

    def __init__(self, all_offsets=config.OFFSETS_USE_ALL):
        """

        :param all_offsets: bool
        True if want to generate all offsets,
        False if only want to load the offsets specified in config.FILE_OFFSETS file
        """
        np.random.seed(config.OFFSETS_SEED)

        self._offsets_u = np.random.randint(-config.OFFSET_MAX, config.OFFSET_MAX, (config.N_FEATURES, 2))
        self._offsets_v = np.random.randint(-config.OFFSET_MAX, config.OFFSET_MAX, (config.N_FEATURES, 2))

        self._feats_ch1 = np.random.randint(0, 3, config.N_FEATURES)
        self._feats_ch2 = np.random.randint(0, 3, config.N_FEATURES)

        self._feats_func = np.random.randint(0, 4, config.N_FEATURES)

    def get(self, image, labels):
        """
        Given a depth image and a set of (x, y) positions,
        get the features of the pixels in the image specified by the positions.

        :param image: uint16 ndarray The depth image to use
        :param positions: List|ndarray of the (x, y) positions to compute the features.

        :return: Tuple list, ndarray
        A list with the positions of the features computed.
        An ndarray of shape (len(positions), len(offsets)) with the features computed in each position.

        """
        h, w = image.shape[:2]
        margin = config.OFFSET_MAX
        features = np.zeros((h * w, config.N_FEATURES+1), dtype=np.int16)

        im = cv2.copyMakeBorder(image, top=margin, bottom=margin, left=margin,
                                right=margin, borderType=cv2.BORDER_REFLECT)

        for nfeat in range(config.N_FEATURES):
            fun = self._feats_func[nfeat]
            y1, x1 = self._offsets_u[nfeat, :]
            y2, x2 = self._offsets_v[nfeat, :]
            ch1 = self._feats_ch1[nfeat]
            ch2 = self._feats_ch2[nfeat]

            A = im[margin + y1:margin + y1 + h, margin + x1:margin + x1 + w, ch1]
            B = im[margin + y2:margin + y2 + h, margin + x2:margin + x2 + w, ch2]

            if fun == 0:
                feat = A

            elif fun == 1:
                feat = A + B

            elif fun == 2:
                feat = A - B

            elif fun == 3:
                feat = np.abs(A - B)

            else:
                raise Exception("Erroneous function")

            features[:, nfeat] = feat.flatten()
        features[:, -1] = labels.flatten()

        return features

    def random_features(self, image, labels, nsamples_class=500):
        tags = np.unique(labels)
        index = np.argwhere(tags == 0)
        tags = np.delete(tags, index)
        if len(tags) == 0:
            # if all labels are background
            return None

        nsamples_tags = [nsamples_class]
        for t in tags:
            positions = np.nonzero(labels == t)
            positions_y, positions_x = positions
            nsamples_tags.append(len(positions_y))
        nsamples = np.min(nsamples_tags)

        all_features = self.get(image, labels)

        list_tag_feats = []
        for i_tag, t in enumerate(tags):
            positions = np.nonzero(labels.flatten() == t)[0]
            idx_samples = np.random.randint(0, len(positions), nsamples)

            tag_features = np.zeros((nsamples, config.N_FEATURES + 1), dtype=np.int16)
            i = 0
            for i_pos in idx_samples:
                tag_features[i, :] = all_features[positions[i_pos], :]
                i += 1
            list_tag_feats.append(tag_features)

        return np.concatenate(list_tag_feats)
