import logging
import os
import pickle

import numpy as np
import sklearn.metrics as metrics
from sklearn.ensemble import RandomForestClassifier

from DatasetsHelpers.DatasetsHelpers import VOC2012Image
from Textons.Config import config


def get(file_clf, fGen, train_files, test_file, trees4file=config.rf_inc_trees_fit,
        max_depth=config.rf_max_depth, min_samples_leaf=config.rf_min_samples_leaf,
        force_clf_creation=False, do_prediction=True, labels=None, image_class=VOC2012Image):

    if os.path.isfile(file_clf) and not force_clf_creation:
        with open(file_clf, 'rb') as handle:
            clf = pickle.load(handle)
    else:
        clf = RandomForestClassifier(
            n_estimators=0,
            class_weight="balanced",
            min_samples_leaf=min_samples_leaf,
            criterion="entropy",
            warm_start=False,
            max_depth=max_depth,
            random_state=10,
            n_jobs=1,
            verbose=0
        )
        logging.info("Training classifier ... ")
        n_images_group = config.DATA_IMS_IN_FILE
        i_ini = 0
        i_end = n_images_group
        while i_end < len(train_files):
            prefeats = None
            for p_im in train_files[i_ini:i_end]:
                im = image_class(p_im)
                if not im.isCorrect:
                    continue
                logging.info("training image: " + im.name_im)
                if prefeats is None:
                    prefeats = fGen.random_features(im.im_lab.astype(np.int16), im.labels)
                else:
                    features = fGen.random_features(im.im_lab.astype(np.int16), im.labels)
                    if features is not None:
                        prefeats = np.concatenate([prefeats, features])

            logging.info("Fitting estimator ...")
            clf.n_estimators += trees4file

            X, y = prefeats[:, :-1], prefeats[:, -1]
            clf.fit(X, y)

            i_ini = i_end
            i_end += n_images_group
        with open(file_clf, 'wb') as handle:
            pickle.dump(clf, handle)

    if do_prediction:
        predict(file_clf, clf, fGen, test_file, labels=labels, image_class=image_class)

    return clf


def predict(file_clf, clf, fGen, test_file, labels=None, image_class=VOC2012Image):
    logging.info("Testing classifier ... ")
    y_pred = []
    y_real = []
    n_test_images = 0
    for p_im in test_file:
        im = image_class(p_im)
        if not im.isCorrect:
            continue

        logging.info("testing image: " + im.name_im)
        features = fGen.random_features(im.im_lab.astype(np.int16), im.labels)
        if features is None:
            continue

        X, y = features[:, :-1], features[:, -1]
        y_pred.extend(clf.predict(X))
        y_real.extend(y)
        n_test_images += 1
    show_stats(y_real, y_pred, labels)

    clf_report = metrics.classification_report(y_real, y_pred, target_names=labels)
    with open(os.path.dirname(file_clf) + "/clf_report.txt", "w") as text_file:
        text_file.write(clf_report)

    logging.debug("Test images:", n_test_images)


def show_stats(y_true, y_pred, labels=None):

    print(metrics.classification_report(y_true, y_pred, target_names=labels))
    precision, recall, f1, support = metrics.precision_recall_fscore_support(
        y_true, y_pred, average='weighted', pos_label=True)
    avg_scores = metrics.precision_recall_fscore_support(y_true, y_pred,
                                                         average="weighted")
    cohenKappa = metrics.cohen_kappa_score(y_true, y_pred)
    print(avg_scores)
    print("Confussion matrix: ")
    print(metrics.confusion_matrix(y_true, y_pred))
    print("Cohen kappa score: " + str(cohenKappa))
    return {
        "F1": f1,
        "Recall(TPR)": recall,
        "Precision": precision,
        "Precision_avg": str(avg_scores[0]),
        "Recall_avg": str(avg_scores[1]),
        "F1_avg": str(avg_scores[2]),
        "Support_total": str(avg_scores[3]),
        "cohen_kappa": cohenKappa
    }
