import logging
import os
import pickle
from glob import glob

import cv2
import numpy as np
import ovnImage as ovni
from sklearn.ensemble import RandomForestClassifier
from tqdm import tqdm

import classifier

from Textons.Config import config
from HOG.VOC2012ImageHog import VOC2012ImageHog
from HOG.HOGFeatures import HOGFeatures

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


config.OFFSET_MAX = 5
config.DATA_PIXELS_CLASS = 100
config.DATA_IMS_IN_FILE = 100
config.N_FEATURES = 1000

config.DATASET = "VOC2012"

VOC2012ImageHog.PATH_DATASET = config.PATH_DATASET


def get_clf(file_clf, fGen, train_files, test_file, trees4file=config.rf_inc_trees_fit,
            max_depth=config.rf_max_depth, min_samples_leaf=config.rf_min_samples_leaf,
            force_clf_creation=False, do_prediction=True, labels=None):
    n_train_images = 0
    if os.path.isfile(file_clf) and not force_clf_creation:
        with open(file_clf, 'rb') as handle:
            clf = pickle.load(handle)
        logging.info("Classifier loaded from file")

    else:
        clf = RandomForestClassifier(
            n_estimators=0,
            class_weight="balanced",
            min_samples_leaf=min_samples_leaf,
            criterion="entropy",
            warm_start=False,
            max_depth=max_depth,
            random_state=10,
            n_jobs=1,
            verbose=0
        )
        logging.info("Training classifier ... ")
        n_images_group = config.DATA_IMS_IN_FILE
        i_ini = 0
        i_end = n_images_group
        while i_end < len(train_files):
            prefeats = None
            for p_im in train_files[i_ini:i_end]:
                im = VOC2012ImageHog(p_im)
                if not im.isCorrect:
                    continue

                print("training image: " + im.name_im)
                # if im.name_im == "2008_002241.png":
                #     fGen.random_features(im, im.getHogLabels())
                #     pass
                if prefeats is None:
                    prefeats = fGen.random_features(im, im.getHogLabels())

                else:
                    features = fGen.random_features(im, im.getHogLabels())
                    if features is not None:
                        prefeats = np.concatenate([prefeats, features])
                    # print(np.unique(prefeats, return_counts=True))
                n_train_images += config.rf_inc_trees_fit

            print("Fitting estimator ...")
            clf.n_estimators += trees4file
            X, y = prefeats[:, :-1], prefeats[:, -1]
            clf.fit(X, y)

            i_ini = i_end
            i_end += n_images_group
        with open(file_clf, 'wb') as handle:
            pickle.dump(clf, handle)

    if do_prediction:
        predict(file_clf, clf, fGen, test_file, labels=labels)

    return clf


def predict(file_clf, clf, fGen, test_file, labels=None):
    logging.info("Testing classifier ... ")
    y_pred = []
    y_real = []
    n_test_images = 0
    for p_im in test_file:
        im = VOC2012ImageHog(p_im)
        if not im.isCorrect:
            continue

        print("testing image: " + im.name_im)
        features = fGen.random_features(im, im.getHogLabels())
        if features is None:
            continue

        X, y = features[:, :-1], features[:, -1]
        y_pred.extend(clf.predict(X))
        y_real.extend(y)
        n_test_images += 1
    classifier.show_stats(y_real, y_pred, labels)

    clf_report = classifier.metrics.classification_report(y_real, y_pred, target_names=labels)
    with open(os.path.dirname(file_clf) + "/clf_report.txt", "w") as text_file:
        text_file.write(clf_report)

    print("Test images:", n_test_images)


def main():
    SAVE_FIGURES = True
    PATH_RESULTS = config.FOLDER_RESULTS + "VOC2012/hog/"
    PATH_RESULTS_IMAGES = PATH_RESULTS + "images/"

    file_clf = PATH_RESULTS + "clf.sav"
    force_clf_creation = False
    do_prediction = True
    ovni.check_dir(PATH_RESULTS)
    ovni.check_dir(PATH_RESULTS_IMAGES)

    path_images = glob(config.PATH_DATASET + "/SegmentationClass/*.png")
    list_train_imgs_folders = path_images[:int(len(path_images) * 0.8)]
    list_test_imgs_folders = path_images[int(len(path_images) * 0.8):]
    
    f = HOGFeatures()
    clf = get_clf(file_clf, f, list_train_imgs_folders, list_test_imgs_folders,
                  do_prediction=do_prediction,
                  trees4file=config.rf_inc_trees_fit, max_depth=config.rf_max_depth,
                  force_clf_creation=force_clf_creation,
                  labels=VOC2012ImageHog.class_names())

    plotter = ovni.MultiPlot(6) if SAVE_FIGURES else ovni.InteractivePlot(6)

    for filename in tqdm(list_test_imgs_folders):
        im = VOC2012ImageHog(filename)
        if im.isCorrect:
            features = f.get(im, im.getHogLabels())
            shape = np.asarray(im.getHogLabels().shape[:2])
            predicted = clf.predict(features[:, :-1])
            proba_mask = np.reshape(predicted, shape)

            plot_images = [
                {
                    "img": im.im_rgb,
                    "title": "original"
                },
                {
                    "img": im.gray,
                    "title": "grayscale"
                },
                {
                    "img": im.hogImage,
                    "title": "HOG"
                },
                {
                    "img": im.labels,
                    "title": "labels",
                    "cmap": "tab20",
                    "colorbar": True
                },
                {
                    "img": im.getHogLabels(),
                    "title": "HOG labels",
                    "cmap": "tab20",
                    "colorbar": True
                },
                {
                    "img": proba_mask,
                    "title": "predictions",
                    "cmap": "tab20",
                    "colorbar": True
                }
            ]

            if SAVE_FIGURES:
                plotter.save_multiplot(PATH_RESULTS_IMAGES + im.name_im.split(".")[0] + ".png", plot_images)

            else:
                plotter.multi(plot_images)


main()
