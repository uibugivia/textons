from glob import glob

import cv2
import numpy as np
from ovnImage import InteractivePlot, MultiPlot, check_dir

import classifier
from DatasetsHelpers.DatasetsHelpers.MSRCv2Image import MSRCv2Image
from Textons.Config import config
from Textons.Features import Features

config.DATASET = "MSRC_v2"
SAVE_FIGURES = True
PATH_RESULTS = config.FOLDER_RESULTS + "MSRC/v1/"
PATH_RESULTS_IMAGES = PATH_RESULTS + "images/"
PATH_RESULTS_PATCHES = PATH_RESULTS + "patches/"

file_clf = PATH_RESULTS + "clf.sav"
force_clf_creation = False
do_prediction = False
check_dir(PATH_RESULTS)
check_dir(PATH_RESULTS_IMAGES)
check_dir(PATH_RESULTS_PATCHES)


class RDFInspect:
    def __init__(self, clf):
        self.clf = clf
        self.patch_w = config.OFFSET_MAX * 2 + 1
        self.leaves_hist = np.zeros((2 ** config.rf_max_depth) * (clf.n_estimators+1))
        self.leaves_patches = np.zeros(((2 ** config.rf_max_depth) * (clf.n_estimators+1),
                                        self.patch_w, self.patch_w, 3), dtype=np.uint64)

    # def tree_paths(self, im, features):
    #     patch_w = config.OFFSET_MAX * 2 + 1
    #     patches_feats = []
    #     h, w = im.classes.shape[:2]
    #     ini_pos = np.array([0, 0])
    #     for i_y in range(patch_w, h, patch_w):
    #         for i_x in range(patch_w, w, patch_w):
    #             patch = im.im_rgb[ini_pos[0]:i_y, ini_pos[1]:i_x]
    #             sample_pos = ini_pos + config.OFFSET_MAX
    #             patch_feats = features[sample_pos[0] * w + sample_pos[1], :]
    #             patches_feats.append(patch_feats)
    #
    #             ini_pos[1] = i_x
    #         ini_pos[0] = i_y
    #
    #     patches_feats = np.asarray(patches_feats)
    #     indicator, n_nodes_ptr = self.clf.decision_path(patches_feats[:, :-1])
    #     for i_tree in range(self.clf.n_estimators):
    #         estimator_ind = indicator[:, n_nodes_ptr[i_tree]:n_nodes_ptr[i_tree + 1]]
    #         for i_sample in range(estimator_ind.shape[0]):
    #             sample_ind = estimator_ind[i_sample, :]
    #
    #             pass

    def tree_leaves(self, im, features):
        patch_w = config.OFFSET_MAX * 2 + 1
        patches_feats = []
        h, w = im.classes.shape[:2]
        ini_pos = np.array([0, 0])
        patches = []
        for i_y in range(patch_w, h, patch_w):
            for i_x in range(patch_w, w, patch_w):
                patch = im.im_rgb[ini_pos[0]:i_y, ini_pos[1]:i_x]

                patches.append(patch)

                sample_pos = ini_pos + config.OFFSET_MAX
                patch_feats = features[sample_pos[0] * w + sample_pos[1], :]
                patches_feats.append(patch_feats)

                ini_pos[1] = i_x
            ini_pos[1] = 0
            ini_pos[0] = i_y

        patches_feats = np.asarray(patches_feats)
        X_leaves = self.clf.apply(patches_feats[:, :-1])
        for i_tree in range(self.clf.n_estimators):
            i_leaves = X_leaves[:, i_tree]
            tree = self.clf.estimators_[i_tree].tree_
            for i_sample, i_leaf in enumerate(i_leaves):
                self.leaves_hist[i_tree*(2**config.rf_max_depth) + i_leaf] += 1
                self.leaves_patches[i_tree*(2**config.rf_max_depth) + i_leaf, :] += patches[i_sample]

    def generate_results(self):
        idx_leaves = np.argsort(self.leaves_hist)[::-1][:256]
        hist = self.leaves_hist[idx_leaves]
        patches = self.leaves_patches[idx_leaves]
        for i in range(len(hist)):
            patches[i] = patches[i] / hist[i]

            cv2.imwrite(PATH_RESULTS_PATCHES + "patch_" + str(i) + ".png",
                        patches[i])

        p_h, p_w, _ = patches[0].shape
        i = 0
        image = np.zeros((16 * p_h, 16 * p_w, 3))
        for i_x in range(p_w, image.shape[1], p_w):
            for i_y in range(p_h, image.shape[0], p_h):
                image[i_y - p_h:i_y, i_x - p_w:i_x] = patches[i]
                i += 1

        cv2.imwrite(PATH_RESULTS + "patches.png",
                    image)


def MAIN():
    path_images = glob(config.PATH_DATASET + "/Images/*.bmp")
    list_train_imgs_folders = path_images[:501]
    list_test_imgs_folders = path_images[501:]

    fGen = Features()

    clf = classifier.get(file_clf, fGen, list_train_imgs_folders, list_test_imgs_folders,
                         do_prediction=do_prediction,
                         trees4file=config.rf_inc_trees_fit, max_depth=config.rf_max_depth,
                         force_clf_creation=force_clf_creation,
                         labels=MSRCv2Image.class_names()[1:],
                         image_class=MSRCv2Image)
    path_results_trees = []
    for i_tree in range(clf.n_estimators):
        path = PATH_RESULTS + "tree_" + str(i_tree) + "/"
        path_results_trees.append(path)
        check_dir(path)

    print("Plotting")
    plotter = MultiPlot(3+clf.n_estimators) if SAVE_FIGURES else InteractivePlot(3)

    config.DATASET = "MSRC_v2"
    MSRCv2Image.PATH_DATASET = config.PATH_DATASET

    inspector = RDFInspect(clf)
    for p_im in list_test_imgs_folders:
        im = MSRCv2Image(p_im)
        print("Plotting image: " + im.name_im)
        if not im.isCorrect:
            continue

        features = fGen.get(im.im_lab.astype(np.int16), im.labels)

        shape = np.asarray(im.labels.shape[:2])
        predicted = clf.predict(features[:, :-1])
        proba_mask = np.reshape(predicted, shape)
        print(np.max(proba_mask))
        plot_images = [
                {
                  "img": im.im_rgb,
                  "title": "original"
                },
                {
                    "img": im.classes,
                    "title": "labels"
                },
            ]

        inspector.tree_leaves(im, features)

        for i_tree, dt in enumerate(clf.estimators_):
            tree_pred = dt.predict(features[:, :-1])
            im_tree_pred = np.reshape(tree_pred, shape)
            print("tree max: ", np.unique(tree_pred))

            plot_images.append({
                    "img": im_tree_pred,
                    "title": "tree " + str(i_tree),
                    "cmap": "tab20",
                    "colorbar": True
            })

        plot_images.append({
                    "img": proba_mask,
                    "title": "predictions",
                    "cmap": "tab20",
                    "colorbar": True
                })

        if SAVE_FIGURES:
            plotter.save_multiplot(PATH_RESULTS_IMAGES + im.name_im.split(".")[0] + ".png", plot_images)
        else:
            plotter.multi(plot_images)

    inspector.generate_results()


MAIN()
