import os
import pickle
import random

import cv2
import numpy as np
import sklearn.metrics as metrics
from ovnImage import InteractivePlot, MultiPlot, check_dir
from sklearn.ensemble import RandomForestClassifier

from DatasetsHelpers.DatasetsHelpers.VOC2012Image import VOC2012Image
from Textons.Config import config
from Textons.Features import Features


def show_stats(y_true, y_pred, labels=None):

    print(metrics.classification_report(y_true, y_pred, target_names=labels))
    precision, recall, f1, support = metrics.precision_recall_fscore_support(y_true, y_pred, average='weighted', pos_label=True)
    avg_scores = metrics.precision_recall_fscore_support(y_true, y_pred,
                                                 average="weighted")
    cohenKappa = metrics.cohen_kappa_score(y_true, y_pred)
    print(avg_scores)
    print("Confussion matrix: ")
    print(metrics.confusion_matrix(y_true, y_pred))
    print("Cohen kappa score: " + str(cohenKappa))
    return {
        "F1": f1,
        "Recall(TPR)": recall,
        "Precision": precision,
        "Precision_avg": str(avg_scores[0]),
        "Recall_avg": str(avg_scores[1]),
        "F1_avg": str(avg_scores[2]),
        "Support_total": str(avg_scores[3]),
        "cohen_kappa": cohenKappa
    }


def generate_random_image():
    source_image = VOC2012Image(config.PATH_DATASET + "/SegmentationClass/2007_001764.png")

    grass = [
        (source_image.im_rgb[300:350, 300:350], source_image.labels[300:350, 300:350]),
        (source_image.im_rgb[200:250, 100:150], source_image.labels[200:250, 100:150]),
        (source_image.im_rgb[250:350, 400:500], source_image.labels[250:350, 400:500]),
        (source_image.im_rgb[300:350, 100:200], source_image.labels[300:350, 100:200]),
    ]
    cowsmask = (source_image.labels != 0).astype(np.uint8)

    im2, contours, hierarchy = cv2.findContours(cowsmask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cows = []
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        cows.append(
            (source_image.im_rgb[y:y + h, x:x + w],
             source_image.labels[y:y + h, x:x + w])
        )

    pieces = [grass, cows]
    sample_image = np.zeros(source_image.im_rgb.shape, dtype=np.uint8)
    sample_mask = np.zeros(source_image.labels.shape, dtype=np.uint8)
    im_h, im_w = sample_image.shape[:2]
    y = 0
    while y < im_h:
        x = 0
        ys = []
        while x < im_w:
            kind = random.randint(0, 1)
            npiece = random.randint(0, len(pieces[kind]) - 1)

            h, w, _ = pieces[kind][npiece][0].shape

            lx = x + w if x + w < im_w else im_w
            ly = y + h if y + h < im_h else im_h
            piece_bgr = pieces[kind][npiece][0][:ly - y, :lx - x]
            piece_mask = pieces[kind][npiece][1][:ly - y, :lx - x]
            sample_image[y:ly, x:lx] = piece_bgr
            sample_mask[y:ly, x:lx] = piece_mask
            x = lx
            ys.append(ly)
        y = np.min(ys)

    return sample_image, sample_mask


def MAIN():
    SAVE_FIGURES = True
    PATH_RESULTS = config.FOLDER_RESULTS + "pieces/"
    PATH_RESULTS_IMAGES = PATH_RESULTS + "images/"

    file_clf = PATH_RESULTS + "clf.sav"
    force_clf_creation = False
    check_dir(PATH_RESULTS)
    check_dir(PATH_RESULTS_IMAGES)

    fGen = Features()
    clf = RandomForestClassifier(
        criterion="entropy",
        max_depth=30,
        warm_start=True,
        n_estimators=0
    )
    print("Training")
    if os.path.isfile(file_clf) and not force_clf_creation:
        with open(file_clf, 'rb') as handle:
            clf = pickle.load(handle)
    else:
        n_images_group = 25
        n_train_images = 50
        i_image = 0
        i_end = n_images_group
        while i_image < n_train_images:
            prefeats = None
            for j in range(n_images_group):
                im, mask = generate_random_image()
                print("training image ... ")
                if prefeats is None:
                    prefeats = fGen.random_features(im.astype(np.int16), mask)

                else:
                    features = fGen.random_features(im.astype(np.int16), mask)
                    if features is not None:
                        prefeats = np.concatenate([prefeats, features])
                i_image += 1
            print("Fitting estimator ...")
            clf.n_estimators += 1

            X, y = prefeats[:, :-1], prefeats[:, -1]
            clf.fit(X, y)

            i_end += n_images_group
            with open(file_clf, 'wb') as handle:
                pickle.dump(clf, handle)

    print("Plotting")
    plotter = MultiPlot(3) if SAVE_FIGURES else InteractivePlot(3)

    for i in range(5):
        im, mask = generate_random_image()

        print("Plotting image ... ")

        features = fGen.get(im.astype(np.int16), mask)

        predicted = clf.predict_proba(features[:, :-1])
        shape = np.asarray(mask.shape[:2]) - 2*config.OFFSET_MAX
        proba_mask = np.reshape(predicted[:, 1], shape)
        proba_mask[1, 1] = 1.0

        plot_images = [
                {
                  "img": im,
                  "title": "original"
                },
                {
                    "img": mask,
                    "title": "labels"
                },
                {
                    "img": proba_mask,
                    "title": "predictions",
                    "cmap": "bwr"
                }
            ]

        if SAVE_FIGURES:
            plotter.save_multiplot(PATH_RESULTS_IMAGES + str(i) + ".png", plot_images)
        else:
            plotter.multi(plot_images)

MAIN()
