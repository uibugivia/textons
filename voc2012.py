from glob import glob

import numpy as np
from ovnImage import InteractivePlot, MultiPlot, check_dir

import classifier
from DatasetsHelpers.DatasetsHelpers.VOC2012Image import VOC2012Image
from Textons.Config import config
from Textons.Features import Features


def MAIN():
    SAVE_FIGURES = True
    PATH_RESULTS = config.FOLDER_RESULTS + "VOC2012/v1/"
    PATH_RESULTS_IMAGES = PATH_RESULTS + "images/"

    config.DATASET = "VOC2012"
    VOC2012Image.PATH_DATASET = config.PATH_DATASET

    file_clf = PATH_RESULTS + "clf.sav"
    force_clf_creation = False
    do_prediction = False
    check_dir(PATH_RESULTS)
    check_dir(PATH_RESULTS_IMAGES)

    path_images = glob(config.PATH_DATASET + "/SegmentationClass/*.png")
    list_train_imgs_folders = path_images[:int(len(path_images) * 0.8)]
    list_test_imgs_folders = path_images[int(len(path_images) * 0.8):]
    print(path_images)
    fGen = Features()

    clf = classifier.get(file_clf, fGen, list_train_imgs_folders, list_test_imgs_folders,
                         do_prediction=do_prediction,
                         trees4file=config.rf_inc_trees_fit, max_depth=config.rf_max_depth,
                         force_clf_creation=force_clf_creation, labels=VOC2012Image.VOC_CLASSES[1:])

    print("Plotting")
    plotter = MultiPlot(3) if SAVE_FIGURES else InteractivePlot(3)

    for p_im in list_test_imgs_folders:
        im = VOC2012Image(p_im)
        print("Plotting image: " + im.name_im)
        if not im.isCorrect:
            continue

        features = fGen.get(im.im_lab.astype(np.int16), im.labels)

        shape = np.asarray(im.labels.shape[:2])
        predicted = clf.predict(features[:, :-1])
        proba_mask = np.reshape(predicted, shape)

        plot_images = [
                {
                  "img": im.im_rgb,
                  "title": "original"
                },
                {
                    "img": im.labels,
                    "title": "labels"
                },
                {
                    "img": proba_mask,
                    "title": "predictions",
                    "cmap": "tab20",
                    "colorbar": True
                }
            ]

        if SAVE_FIGURES:
            plotter.save_multiplot(PATH_RESULTS_IMAGES + im.name_im, plot_images)
        else:
            plotter.multi(plot_images)


MAIN()
